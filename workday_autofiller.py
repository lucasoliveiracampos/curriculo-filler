# doc:
# - https://evertutorial.com/articles/PythonAutomation/AutomatedFormFilling
# - https://selenium-python.readthedocs.io/locating-elements.html
# - https://pythonexamples.org/python-selenium-select-a-radio-button/
# - https://scrapeops.io/selenium-web-scraping-playbook/python-selenium-wire/

import json
from time import sleep
from seleniumwire import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

def select_in_dropbox(element_id, search_click):
    dropbox = driver.find_element(By.ID, element_id)
    dropbox.click()
    sleep(1)
    dropbox_dropbox_id = dropbox.get_attribute('aria-controls')
    dropbox_dropbox = driver.find_element(By.ID, dropbox_dropbox_id)
    dropbox_list = dropbox_dropbox.find_elements(By.TAG_NAME, 'li')
    for li in dropbox_list:
        if li.text == search_click:
            li.click()
            break


def next():
    # https://www.w3schools.com/cssref/css_selectors.php
    driver.find_element(By.CSS_SELECTOR, 'button[data-automation-id="bottom-navigation-next-button"]').click()
    sleep(5)


with open("./data.json", "r") as f:
    personal_info = json.load(f)

driver  = webdriver.Chrome()
driver.get(url='https://slihrms.wd3.myworkdayjobs.com/Careers/job/IEDublinSwords/Graduate-Engineer---Ireland--Other-Colleges-_R-082393-2/apply?utm_source=external%2Bcareers&source=external%2Bcareers')
#driver.get(url='https://careers.snclavalin.com/job/graduate-engineer-ireland-in-dublin-jid-25585')
#sleep(10)
# driver.find_element(By.PARTIAL_LINK_TEXT, 'Apply').click()
sleep(10)
driver.find_element(By.PARTIAL_LINK_TEXT, 'Apply Manually').click()
sleep(10)
select_in_dropbox('input-1', 'LinkedIn Job posting')
driver.find_element(By.ID, '2').click()
select_in_dropbox('input-4', 'Mrs.')
# sleep(22)
driver.find_element(By.ID, 'input-5').send_keys(personal_info['name'])
driver.find_element(By.ID, 'input-6').send_keys(personal_info['middle_name'])
driver.find_element(By.ID, 'input-7').send_keys(personal_info['surname'])
driver.find_element(By.ID, 'input-9').send_keys(personal_info['full_address'])
driver.find_element(By.ID, 'input-10').send_keys(personal_info['eircode'])
select_in_dropbox('input-11', personal_info['city'])
driver.find_element(By.ID, 'input-12').send_keys(personal_info['email'])
driver.find_element(By.ID, 'input-15').send_keys(personal_info['phone'])
next()
driver.find_element(By.CSS_SELECTOR, 'input[data-automation-id="file-upload-input-ref"]').send_keys(personal_info['cv_location'])
next()
driver.find_element(By.ID, 'input-18').click()
next()
import pdb
pdb.set_trace()
#driver.find_element(By.CSS_SELECTOR, 'input[data-automation-id="bottom-navigation-next-button"]').click()

